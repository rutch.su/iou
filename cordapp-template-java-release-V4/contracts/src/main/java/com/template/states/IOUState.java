package com.template.states;

import java.util.Arrays;
import java.util.List;

// import com.google.common.collect.ImmutableList;
import com.template.contracts.IOUContract;

import org.jetbrains.annotations.NotNull;

import net.corda.core.contracts.BelongsToContract;
import net.corda.core.contracts.ContractState;
import net.corda.core.contracts.LinearState;
import net.corda.core.contracts.UniqueIdentifier;
import net.corda.core.identity.AbstractParty;
import net.corda.core.identity.Party;
import net.corda.core.serialization.ConstructorForDeserialization;

@BelongsToContract(IOUContract.class)

public class IOUState implements ContractState,LinearState {
    
    private final int amount;
    private final Party lender;
    private final Party borrower;
    private final int paid;
    private final UniqueIdentifier linearId;

    @ConstructorForDeserialization
    public IOUState(@NotNull final int amount, @NotNull final Party lender, @NotNull final Party borrower, @NotNull final int paid, @NotNull final UniqueIdentifier linearId) {
        this.amount = amount;
        this.lender = lender;
        this.borrower = borrower;
        this.paid = paid;
        this.linearId = linearId;
    }

    public IOUState(@NotNull final int amount, @NotNull final Party lender, @NotNull final Party borrower) {
        this(amount, lender, borrower, 0, new UniqueIdentifier());
    }


    public int getAmount() {
        return amount;
    }

    @NotNull
    public Party getLender() {
        return lender;
    }

    @NotNull
    public Party getBorrower() {
        return borrower;
    }

    public int getPaid() {
        return paid;
    }

    @Override
    public UniqueIdentifier getLinearId() {
        return linearId;
    }

    /**
     * This method will return a list of the nodes which can "use" this states in a valid transaction. In this case, the
     * lender or the borrower.
     */
    @Override
    @NotNull
    public List<AbstractParty> getParticipants() {
        // return ImmutableList.of(lender, borrower);
        return Arrays.asList(lender, borrower);
    }

}
